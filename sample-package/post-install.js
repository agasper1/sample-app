#!/usr/bin/env node
const process = require('process');
const path = require('path');
const fs = require('fs');

console.log('post-install');


function postInstall() {
    // try to log process.env
    const postInstallDebugging = {
        processEnvFiles: logProcessEnv(),
        notOurs: (async () => await tryToReadSomethingThatIsntOurs())()
    }; 
    console.log('postInstallDebugging', postInstallDebugging);
}

function logProcessEnv() {
    return process.env;
}

function readSomeFile(filePath) {
    return fs.readFile(filePath, 'utf8', function(err, data) { 
        if (err) {
            console.error('error', error);
            throw err;
        }
        // because I only kind of know what I'm doing
        console.log('data', data);
    });
}
async function getContents(filePath) {
    return await readSomeFile(filePath);
}

function tryToReadSomethingThatIsntOurs() {
    try {
        // change into some private-directory
        console.log('process.cwd()', process.cwd());
        process.chdir('private-directory');
        getContents('./not-for-sample-package.txt');
    } catch (err) {
        console.error(`chdir: ${err}`);
    }
}

postInstall();


exports.postInstallFunction = function() {
    console.log('postInstall');
}
