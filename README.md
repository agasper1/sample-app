## Installation

1. Install `sample-package`
2. `sample-package` defines a `postinstall` step and executes `post-install.js`
```
{
    "name": "sample-package",
    "main": "sample-package.js",
    "scripts": {
        "postinstall": "node ./post-install.js"
    }
}
```
3. `post-install` tries to get up to no good


The repo has a hidden variable
```
EXAMPLE_HIDDEN_VARIABLE="kitten_mittens"
```